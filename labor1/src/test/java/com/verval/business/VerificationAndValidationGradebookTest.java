package com.verval.business;

import com.verval.business.business.VerificationAndValidationGradeCalculator;
import com.verval.business.business.VerificationAndValidationGradebook;
import com.verval.business.exception.NotEligibleStudentException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.mockito.internal.matchers.Any;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


public class VerificationAndValidationGradebookTest {

    @InjectMocks
    VerificationAndValidationGradebook spiedBook;

    @Mock
    ArrayList<VerificationAndValidationGradeCalculator> vAndVGradeCalculators;

    @Mock
    VerificationAndValidationGradeCalculator calculatorMock;

    @Mock
    VerificationAndValidationGradeCalculator realCalculator;

    @Captor
    ArgumentCaptor<VerificationAndValidationGradeCalculator> calculatorArgumentCaptor;
    @Captor
    ArgumentCaptor<Boolean> exceptionArgumentCaptor;

    @BeforeEach
    public void init() {
        realCalculator = new VerificationAndValidationGradeCalculator();
        spiedBook = Mockito.spy(new VerificationAndValidationGradebook(15));
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenAddingCalculatorCheckEligibility(){
        when(calculatorMock.getYearOfStudy(false)).thenReturn(10);
        Assertions.assertThrows(NotEligibleStudentException.class,()->spiedBook.addvAndVGradeCalculator(calculatorMock,false));
    }

    @Test
    public void whenAddingCalculatorCheckCount(){
        when(calculatorMock.getYearOfStudy(false)).thenReturn(16);
        spiedBook.addvAndVGradeCalculator(calculatorMock,false);
        Mockito.verify(vAndVGradeCalculators).add(any());
    }

    @Test
    public void whenGettingPassingCountCheckPassing(){
        when(realCalculator.getYearOfStudy(false)).thenReturn(16);
        doReturn(false).when(realCalculator).isPassing();
        spiedBook.addvAndVGradeCalculator(realCalculator,false);
        Assertions.assertEquals(0,spiedBook.getCountOfPassingGrades());
    }

    @Test
    public void whenGettingAverageCheckAverage(){
        when(calculatorMock.getYearOfStudy(false)).thenReturn(17);
        spiedBook.addvAndVGradeCalculator(calculatorMock,false);
        Mockito.verify(spiedBook).addvAndVGradeCalculator(calculatorArgumentCaptor.capture(),exceptionArgumentCaptor.capture());
        Assertions.assertEquals(17,calculatorArgumentCaptor.getValue().getYearOfStudy(exceptionArgumentCaptor.getValue()));
    }

}

