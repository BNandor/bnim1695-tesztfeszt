package com.verval.business;

import com.verval.business.business.VerificationAndValidationGradeCalculator;
import com.verval.business.exception.InvalidPointException;
import com.verval.business.model.GradeType;
import com.verval.business.model.Point;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import java.util.ArrayList;

public class VerificationAndValidationGradeCalculatorTests {
    private VerificationAndValidationGradeCalculator v;

    @BeforeAll()
    private static void wellHelloThere() {
        System.out.println("Well hello there, let us run some tests");
    }

    @BeforeEach
    void initTest() {
        v = new VerificationAndValidationGradeCalculator();
    }

    @Test
    @DisplayName("failWithoutExam")
    void failWithoutExam() {
        Point p1 = new Point(4, 10, GradeType.Laboratory);
        Point p5 = new Point(6, 10, GradeType.Laboratory);
        Point p2 = new Point(5, 10, GradeType.Lecture);
        Point p3 = new Point(10, 10, GradeType.Seminar);
        Point p4 = new Point(5, 10, GradeType.Lecture);

        v.addPoint(p1);
        v.addPoint(p2);
        v.addPoint(p3);
        v.addPoint(p4);
        v.addPoint(p5);

        Assertions.assertFalse(v.isPassing());
    }

    @Test
    @DisplayName("isPassing")
    void passing() {
        Point p1 = new Point(10, 10, GradeType.Laboratory);
        Point p2 = new Point(10, 10, GradeType.Lecture);
        Point p3 = new Point(10, 10, GradeType.Seminar);
        Point p4 = new Point(10, 10, GradeType.Exam);

        v.addPoint(p1);
        v.addPoint(p2);
        v.addPoint(p3);
        v.addPoint(p4);
        Assertions.assertTrue(v.isPassing());
    }

    @Test
    @DisplayName("pointsSumUp")
    void pointsSumUp() {
        Point p1 = new Point(1, 10, GradeType.Laboratory);
        Point p2 = new Point(2, 10, GradeType.Laboratory);
        Point p3 = new Point(3, 10, GradeType.Laboratory);
        Point p4 = new Point(4, 10, GradeType.Laboratory);

        Point p5 = new Point(1, 10, GradeType.Seminar);
        Point p6 = new Point(2, 10, GradeType.Seminar);
        Point p7 = new Point(3, 10, GradeType.Seminar);
        Point p8 = new Point(4, 10, GradeType.Seminar);

        Point p9 = new Point(1, 10, GradeType.Lecture);
        Point p10 = new Point(2, 10, GradeType.Lecture);
        Point p11 = new Point(3, 10, GradeType.Lecture);
        Point p12 = new Point(4, 10, GradeType.Lecture);

        Point ex = new Point(10, 10, GradeType.Exam);

        v.addPoint(p1);
        v.addPoint(p2);
        v.addPoint(p3);
        v.addPoint(p4);
        v.addPoint(p5);
        v.addPoint(p6);
        v.addPoint(p7);
        v.addPoint(p8);
        v.addPoint(p9);
        v.addPoint(p10);
        v.addPoint(p11);
        v.addPoint(p12);
        v.addPoint(ex);
        Assertions.assertEquals(5, v.getGrade());
    }

    @ParameterizedTest(name = "validPoints")
    @CsvSource({
            "-9123,",
            "-1,",
            "49,",
            "-1111,"
    })
    void validPoints(int pointval) {
        Point p1 = new Point(pointval, 10, GradeType.Laboratory);
//        Assertions.assertThrows(InvalidPointException.class, () -> v.validatePoint(p1));
    }

    @Test
    @DisplayName("correctPointOrder")
    void correctPoints() {
        double[] pointsOrder = new double[]{10,4, 6, 5, 10, 5};
        Point p1 = new Point(4, 10, GradeType.Laboratory);
        Point p2 = new Point(6, 10, GradeType.Laboratory);
        Point p3 = new Point(5, 10, GradeType.Lecture);
        Point p4 = new Point(10, 10, GradeType.Seminar);
        Point p5 = new Point(5, 10, GradeType.Lecture);

        v.addPoint(p1);
        v.addPoint(p2);
        v.addPoint(p3);
        v.addPoint(p4);
        v.addPoint(p5);
        ArrayList<Point> points = v.getPoints();
        double [] testPoints = new double[points.size()];
        int i=0;
        for(Point point:points){
            testPoints[i]=point.getPoint();
            ++i;
        }
        Assertions.assertArrayEquals(testPoints,pointsOrder);
    }

}
